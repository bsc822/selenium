﻿using System;
using SeleniumTests;
using static SeleniumTests.Testdvr;
using System.Diagnostics;

namespace D2GoSelenium
{
    class Program
    {
        #region Properties
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
        private const int SW_MINIMIZE = 6;
        private const int SW_MAXIMIZE = 3;
        private const int SW_RESTORE = 9;
        #endregion

        static void Main(string[] args)
        {
            Console.TreatControlCAsInput = false;
            Console.CancelKeyPress += new ConsoleCancelEventHandler(BreakHandler);
            Console.Clear();
            Console.CursorVisible = false;

            string[] months = { "Production", "UAT", "Dev", "Localhost"};
            WriteColorString("Choose Level using down and up arrow keys and press enter", 12, 20, ConsoleColor.Black, ConsoleColor.White);
            Int32 choice = ChooseListBoxItem(months, 34, 3, ConsoleColor.Blue, ConsoleColor.White);
            // do something with choice
            SiteToTest domainToChk = (SiteToTest) Enum.Parse(typeof(SiteToTest), months[choice - 1]);

            WriteColorString("You chose " + months[choice - 1] + ".", 21, 22, ConsoleColor.Black, ConsoleColor.White);
            
            Testdvr Testdvr = new Testdvr(domainToChk);
            Testdvr.SetupTest();

            IntPtr winHandle = Process.GetCurrentProcess().MainWindowHandle;
            ShowWindow(winHandle, SW_MINIMIZE);

            Console.WriteLine("Run XPath Extension by clicking button then hold the \"Shift\" button while hover with the mouse");

            //Different tests
            //https://stackoverflow.com/questions/11908249/debugging-element-is-not-clickable-at-point-error

            Testdvr.RegisterAccount();
            Testdvr.ArtworkAddtest();
            Testdvr.SearchAndBuy();
            Testdvr.writeProductReviews();

            Testdvr.TeardownTest();

            ShowWindow(winHandle, SW_RESTORE);

            WriteColorString($"Test done!! {DateTime.Now.ToLocalTime()}", 21, 22, ConsoleColor.Black, ConsoleColor.White);

            Console.ReadKey();
            CleanUp();
        }

        #region Helper Methods
        public static Int32 ChooseListBoxItem(String[] items, Int32 ucol, Int32 urow, ConsoleColor back, ConsoleColor fore)
        {
            Int32 numItems = items.Length;
            Int32 maxLength = items[0].Length;
            for (Int32 i = 1; i < numItems; i++)
            {
                if (items[i].Length > maxLength)
                {
                    maxLength = items[i].Length;
                }
            }
            Int32[] rightSpaces = new Int32[numItems];
            for (Int32 i = 0; i < numItems; i++)
            {
                rightSpaces[i] = maxLength - items[i].Length + 1;
            }
            Int32 lcol = ucol + maxLength + 3;
            Int32 lrow = urow + numItems + 1;
            DrawBox(ucol, urow, lcol, lrow, back, fore, true);
            WriteColorString(" " + items[0] + new String(' ', rightSpaces[0]), ucol + 1, urow + 1, fore, back);
            for (Int32 i = 2; i <= numItems; i++)
            {
                WriteColorString(items[i - 1], ucol + 2, urow + i, back, fore);
            }
            ConsoleKeyInfo cki;
            Char key;
            Int32 choice = 1;

            while (true)
            {
                cki = Console.ReadKey(true);
                key = cki.KeyChar;
                if (key == '\r') // enter 
                {
                    return choice;
                }
                else if (cki.Key == ConsoleKey.DownArrow)
                {
                    WriteColorString(" " + items[choice - 1] + new string(' ', rightSpaces[choice - 1]), ucol + 1, urow + choice, back, fore);
                    if (choice < numItems)
                    {
                        choice++;
                    }
                    else
                    {
                        choice = 1;
                    }
                    WriteColorString(" " + items[choice - 1] + new string(' ', rightSpaces[choice - 1]), ucol + 1, urow + choice, fore, back);
                }
                else if (cki.Key == ConsoleKey.UpArrow)
                {
                    WriteColorString(" " + items[choice - 1] + new string(' ', rightSpaces[choice - 1]), ucol + 1, urow + choice, back, fore);
                    if (choice > 1)
                    {
                        choice--;
                    }
                    else
                    {
                        choice = numItems;
                    }
                    WriteColorString(" " + items[choice - 1] + new string(' ', rightSpaces[choice - 1]), ucol + 1, urow + choice, fore, back);
                }
            }
        }
        public static void DrawBox(Int32 ucol, Int32 urow, Int32 lcol, Int32 lrow, ConsoleColor back, ConsoleColor fore, bool fill)
        {
            const char Horizontal = '\u2500';
            const char Vertical = '\u2502';
            const char UpperLeftCorner = '\u250c';
            const char UpperRightCorner = '\u2510';
            const char LowerLeftCorner = '\u2514';
            const char LowerRightCorner = '\u2518';
            string fillLine = fill ? new string(' ', lcol - ucol - 1) : "";
            SetColors(back, fore);
            // draw top edge 
            Console.SetCursorPosition(ucol, urow);
            Console.Write(UpperLeftCorner);
            for (Int32 i = ucol + 1; i < lcol; i++)
            {
                Console.Write(Horizontal);
            }
            Console.Write(UpperRightCorner);
            // draw sides 
            for (Int32 i = urow + 1; i < lrow; i++)
            {
                Console.SetCursorPosition(ucol, i);
                Console.Write(Vertical);
                if (fill) Console.Write(fillLine);
                Console.SetCursorPosition(lcol, i);
                Console.Write(Vertical);
            }
            // draw bottom edge 
            Console.SetCursorPosition(ucol, lrow);
            Console.Write(LowerLeftCorner);
            for (Int32 i = ucol + 1; i < lcol; i++)
            {
                Console.Write(Horizontal);
            }
            Console.Write(LowerRightCorner);
        }
        public static void WriteColorString(string s, Int32 col, Int32 row, ConsoleColor back, ConsoleColor fore)
        {
            SetColors(back, fore);
            // write string 
            Console.SetCursorPosition(col, row);
            Console.WriteLine(s);
            Console.WriteLine();
        }
        public static void SetColors(ConsoleColor back, ConsoleColor fore)
        {
            Console.BackgroundColor = back;
            Console.ForegroundColor = fore;
        }
        public static void CleanUp()
        {
            Console.ResetColor();
            Console.CursorVisible = true;
            Console.Clear();
        }
        private static void BreakHandler(object sender, ConsoleCancelEventArgs args)
        {
            // exit gracefully if Control-C or Control-Break pressed 
            CleanUp();
        }
        #endregion
    }
}
