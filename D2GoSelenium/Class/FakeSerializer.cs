﻿using StackExchange.Redis.Extensions.Core;
using System;
using System.Threading.Tasks;

namespace D2GoSelenium.Class
{
    public class FakeSerializer : ISerializer
    {

        T ISerializer.Deserialize<T>(byte[] serializedObject)
        {
            throw new NotImplementedException();
        }

        object ISerializer.Deserialize(byte[] serializedObject)
        {
            throw new NotImplementedException();
        }

        Task<T> ISerializer.DeserializeAsync<T>(byte[] serializedObject)
        {
            throw new NotImplementedException();
        }

        Task<object> ISerializer.DeserializeAsync(byte[] serializedObject)
        {
            throw new NotImplementedException();
        }

        byte[] ISerializer.Serialize(object item)
        {
            throw new NotImplementedException();
        }

        Task<byte[]> ISerializer.SerializeAsync(object item)
        {
            throw new NotImplementedException();
        }
    }
}
