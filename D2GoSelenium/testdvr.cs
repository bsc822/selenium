using System;
using System.Text;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Drawing;
using System.Windows.Forms;
using OpenQA.Selenium.Remote;
using System.Diagnostics;
using System.Linq;
using System.Globalization;
using OpenQA.Selenium.Chrome;
using System.Reflection;
using System.Data.SqlClient;
using System.Configuration;
using StackExchange.Redis;
using StackExchange.Redis.Extensions.Core;
using D2GoSelenium.Class;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;

namespace SeleniumTests
{
    public class Testdvr
    {
        #region properties/instance variables
        private IWebDriver driver;
        private StringBuilder verificationErrors;
        private string baseURL;
        private bool acceptNextAlert = true;
        private Process chromedriverProcess;
        private String tmpEmailAddress;
        private String tmpEmailAddPW;
        private string _order;
        private string _prodId;
        private String publicIp;

        public enum SiteToTest
        {
            Production = 1,
            UAT,
            Dev,
            Localhost,
        };

        String firefoxExtPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FirefoxExtensions");
        String filesToUpload = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FilesToUpload");
        #endregion

        #region Constructor
        public Testdvr(SiteToTest siteTotest)
        {
            switch (siteTotest)
            {
                case SiteToTest.Production:
                    baseURL = "https://www.displays2go.com";
                    break;
                case SiteToTest.UAT:
                    baseURL = "http://hs1.test.d2go.net";
                    break;
                case SiteToTest.Dev:
                    baseURL = "http://hs1.test.d2go.net";
                    break;
                case SiteToTest.Localhost:
                    baseURL = "http://localhost:53899";
                    break;
            }
        }
        #endregion

        #region Setup/TearDown
        public void SetupTest()
        {
            //var catsd = Screen.AllScreens;
            //Rectangle area = Screen.AllScreens[2].WorkingArea;

            //FirefoxExtensions

            //FirefoxOptions options = new FirefoxOptions();
            //options.BrowserExecutableLocation = @"C:\Program Files\Mozilla Firefox\firefox.exe";
            ////options.Profile = new FirefoxProfile(@"C:\Users\Christian.Waldron\AppData\Roaming\Mozilla\Firefox\Profiles\2efzouig.default");

            ////FirefoxBinary ffBinary = new FirefoxBinary(@"C:\Program Files\Mozilla Firefox\firefox.exe");
            //FirefoxProfile profile = new FirefoxProfile();

            ////profile.AddExtension(Path.Combine(firefoxExtPath, @"firebug@software.joehewitt.com.xpi"));
            //profile.AddExtension(Path.Combine(firefoxExtPath, "selenium_ide-2.9.1-fx.xpi")); //Run: Cntrl + Alt + S

            ConsoleColor foregroundColor = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("*To run \"Selenium IDE in Firefox\" enter the shortcut: \"Cntrl + Alt + S\"*");
            Console.ForegroundColor = foregroundColor;
            //profile.AddExtension(Path.Combine(firefoxExtPath, @"selenium_ide_buttons@egarracingteam.com.ar.xpi"));



            //

            //String ext = "extensions.firebug.";
            //profile.SetPreference(ext + "currentVersion", "2.0.19");
            ////profile.setPreference(ext1 + "currentVersion", "0.9.7");
            //profile.SetPreference(ext + "allPagesActivation", "on");
            //profile.SetPreference(ext + "defaultPanelName", "net");
            //profile.SetPreference(ext + "net.enableSites", true);

            //firefoxProfile.AddExtension(new File(firepathPath))

            //driver = new FirefoxDriver(ffBinary, firefoxProfile);


            //options.Profile = profile;

            Process.GetProcessesByName("chromedriver").Skip(1).ToList().ForEach(x => x.Kill());


            //foreach (Process process in Process.GetProcessesByName("chromedriver"))
            //    process.Kill();

            if (!Process.GetProcessesByName("chromedriver").Any())
            {
                ProcessStartInfo startInfo = new ProcessStartInfo()
                {
                    FileName = "chromedriver.exe",
                    WindowStyle = ProcessWindowStyle.Minimized,
                };

                chromedriverProcess = Process.Start(startInfo);
                chromedriverProcess.StartInfo.WindowStyle = ProcessWindowStyle.Minimized;
            }
            else
                chromedriverProcess = Process.GetProcessesByName("chromedriver").FirstOrDefault();


            //----------------------------

            String outPutDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            ChromeOptions options = new ChromeOptions();
            options.AddExtension(outPutDirectory + @"\ChromeExtensions\2.0.2_0.crx");

            driver = new RemoteWebDriver(new Uri("http://localhost:9515"), options.ToCapabilities(), TimeSpan.FromSeconds(25)) { Url = "http://www.google.com" };
            //driver.Url = "http://www.google.com";

            //driver = new FirefoxDriver(options);
            //baseURL = "https://www.displays2go.com";
            //baseURL = "http://hs1.test.d2go.net";
            verificationErrors = new StringBuilder();


            driver.Manage().Window.Position = new Point(Screen.PrimaryScreen.Bounds.Size.Width, 0);
            driver.Manage().Window.Maximize();


            tmpEmailAddress = $"{Guid.NewGuid().ToString().ToUpper().Replace("-", "")}@Displays2GoTest.com";
            tmpEmailAddPW = "displays2go";

        }

        public void TeardownTest()
        {
            try
            {
                driver.Close();
                driver.Quit();
                chromedriverProcess.Kill();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
            //Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual("", verificationErrors.ToString());
        }
        #endregion

        #region Test Methods
        public void RegisterAccount()
        {
            publicIp = GetPublicIPAddress().Replace(':', '-');
            driver.Navigate().GoToUrl("https://www.google.com/");

            ResetCapthaRedisKey("redis-gpaweb.awsdev.d2gstores.com", 6379);

            driver.Navigate().GoToUrl(baseURL + "/");
            driver.FindElement(By.Id("hlp-register-link")).Click();
            driver.FindElement(By.Id("FirstName")).Clear();
            driver.FindElement(By.Id("FirstName")).SendKeys("John");
            driver.FindElement(By.Id("LastName")).Clear();
            driver.FindElement(By.Id("LastName")).SendKeys("Test");
            driver.FindElement(By.Id("EmailAddress")).Clear();
            driver.FindElement(By.Id("EmailAddress")).SendKeys(tmpEmailAddress);
            driver.FindElement(By.Id("Password")).Clear();
            driver.FindElement(By.Id("Password")).SendKeys(tmpEmailAddPW);
            driver.FindElement(By.Id("ConfirmPassword")).Clear();
            driver.FindElement(By.Id("ConfirmPassword")).SendKeys("BadPassword");
            new SelectElement(driver.FindElement(By.Id("IndustryType"))).SelectByText("Healthcare (Hospital, Medical Office, Pharmaceutical)");
            
            driver.FindElement(By.XPath("(//button[@type='submit'])[3]")).Click();
            Thread.Sleep(1500);

            driver.FindElement(By.Id("Password")).Clear();
            driver.FindElement(By.Id("Password")).SendKeys("123456789");
            driver.FindElement(By.Id("ConfirmPassword")).Clear();
            driver.FindElement(By.Id("ConfirmPassword")).SendKeys("123456789");
            new SelectElement(driver.FindElement(By.Id("IndustryType"))).SelectByText("Hospitality (Restaurant, Hotel, Resort, Casino, Conference Center)");            
            driver.FindElement(By.XPath("(//button[@type='submit'])[3]")).Click();
            Thread.Sleep(1500);

            driver.FindElement(By.Id("Password")).Clear();
            driver.FindElement(By.Id("Password")).SendKeys(tmpEmailAddPW);
            driver.FindElement(By.Id("ConfirmPassword")).Clear();
            driver.FindElement(By.Id("ConfirmPassword")).SendKeys(tmpEmailAddPW);         
            driver.FindElement(By.XPath("(//button[@type='submit'])[3]")).Click();
            Thread.Sleep(1500);

            VerifyEmailAddress();

            //----------------------------

            driver.FindElement(By.Id("EmailAddress")).Clear();
            driver.FindElement(By.Id("EmailAddress")).SendKeys(tmpEmailAddress);
            driver.FindElement(By.Id("Password")).Clear();
            driver.FindElement(By.Id("Password")).SendKeys(tmpEmailAddPW);
            driver.FindElement(By.Id("RememberMe")).Click();
            driver.FindElement(By.XPath("(//button[@type='submit'])[3]")).Click();
            Thread.Sleep(1000);

            //---------------------------------------


            driver.FindElement(By.LinkText("Add Or Modify Billing Addresses")).Click();
            driver.FindElement(By.Id("BillCompanyName")).Clear();
            driver.FindElement(By.Id("BillCompanyName")).SendKeys("My Company");
            driver.FindElement(By.Id("BillFirstName")).Clear();
            driver.FindElement(By.Id("BillFirstName")).SendKeys("John");
            driver.FindElement(By.Id("BillLastName")).Clear();
            driver.FindElement(By.Id("BillLastName")).SendKeys("test");
            driver.FindElement(By.Id("BillAddress1")).Clear();
            driver.FindElement(By.Id("BillAddress1")).SendKeys("81 commerce dr");
            driver.FindElement(By.Id("BillCity")).Clear();
            driver.FindElement(By.Id("BillCity")).SendKeys("Fall River");
            new SelectElement(driver.FindElement(By.Id("BillStateProvince"))).SelectByText("Maine");
            new SelectElement(driver.FindElement(By.Id("BillStateProvince"))).SelectByText("Maryland");
            new SelectElement(driver.FindElement(By.Id("BillStateProvince"))).SelectByText("Massachusetts");
            driver.FindElement(By.Id("BillPostalCode")).Clear();
            driver.FindElement(By.Id("BillPostalCode")).SendKeys("02720");
            driver.FindElement(By.Id("BillPhoneNumber")).Clear();
            driver.FindElement(By.Id("BillPhoneNumber")).SendKeys("508-555-1234");
            driver.FindElement(By.XPath("(//button[@type='submit'])[3]")).Click();
            driver.FindElement(By.Id("BillCompanyName")).Clear();
            driver.FindElement(By.Id("BillCompanyName")).SendKeys("Cool COmpany");
            driver.FindElement(By.Id("BillFirstName")).Clear();
            driver.FindElement(By.Id("BillFirstName")).SendKeys("Tony");
            driver.FindElement(By.Id("BillLastName")).Clear();
            driver.FindElement(By.Id("BillLastName")).SendKeys("Stark");
            driver.FindElement(By.Id("BillAddress1")).Clear();
            driver.FindElement(By.Id("BillAddress1")).SendKeys("320 Airport Rd");
            driver.FindElement(By.Id("BillCity")).Clear();
            driver.FindElement(By.Id("BillCity")).SendKeys("Fall River");
            new SelectElement(driver.FindElement(By.Id("BillStateProvince"))).SelectByText("Massachusetts");
            driver.FindElement(By.Id("BillPostalCode")).Clear();
            driver.FindElement(By.Id("BillPostalCode")).SendKeys("02720");
            driver.FindElement(By.Id("BillPhoneNumber")).Clear();
            driver.FindElement(By.Id("BillPhoneNumber")).SendKeys("508-555-8888");
            driver.FindElement(By.XPath("(//button[@type='submit'])[3]")).Click();
            driver.FindElement(By.XPath("//div[@id='billing-addresses-container']/div[2]/div[2]/div/div[2]/div/a")).Click();
            driver.FindElement(By.Id("BillCompanyName")).Clear();
            driver.FindElement(By.Id("BillCompanyName")).SendKeys("Dunkin Donuts");
            driver.FindElement(By.Id("BillAddress2")).Clear();
            driver.FindElement(By.Id("BillAddress2")).SendKeys("1");
            driver.FindElement(By.XPath("(//button[@type='submit'])[3]")).Click();
            driver.FindElement(By.XPath("//div[@id='billing-addresses-container']/div[2]/div[2]/div/div[2]/div/a[2]")).Click();
            try
            {
                Assert.AreEqual("The address has been successfully deleted.", driver.FindElement(By.CssSelector("div.alert.alert-success")).Text);
            }
            catch (Exception e)
            {
                verificationErrors.Append(e.Message);
            }

            BringElementIntoView(By.LinkText("Return To My Account"));

            driver.FindElement(By.LinkText("Return To My Account")).Click();

            driver.FindElement(By.LinkText("Modify Email Notification Preferences")).Click();
            driver.FindElement(By.Id("isSubscribedToUserNotifications")).Click();
            driver.FindElement(By.XPath("(//button[@type='submit'])[3]")).Click();
            driver.FindElement(By.Id("account-dropdown")).Click();
            driver.FindElement(By.LinkText("Log Out")).Click();




            driver.Navigate().GoToUrl("http://www.google.com");

        }

        public void ArtworkAddtest()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(45));
            ResetCapthaRedisKey("redis-gpaweb.awsdev.d2gstores.com", 6379);

            driver.Navigate().GoToUrl(baseURL + "/");
            driver.Navigate().GoToUrl(baseURL + "/P-19998/Popup-Banner-Includes-A-Full-Color-Custom-Graphic?st=Category&sid=1897");
            driver.FindElement(By.Id("customizations-toggle")).Click();
            driver.FindElement(By.Name("productOptionId[0]")).Click();

            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.Id("sku"), "ECBNRSTD33"));

            wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"add-to-cart-form\"]/div/input")));
            driver.FindElement(By.XPath("//*[@id=\"add-to-cart-form\"]/div/input")).Clear();
            driver.FindElement(By.XPath("//*[@id=\"add-to-cart-form\"]/div/input")).SendKeys("2");
            driver.FindElement(By.XPath("//*[@id=\"add-to-cart-form\"]/div/button")).Click();
            
            WaitForElementExistance(By.CssSelector("#main-search-bar > div.input-group.search-box > span.twitter-typeahead > input[name=\"q\"]"));

            By searchInputBox = By.CssSelector("#main-search-bar > div.input-group.search-box > span.twitter-typeahead > input[name=\"q\"]");
            wait.Until(ExpectedConditions.ElementToBeClickable(searchInputBox));

            driver.FindElement(searchInputBox).Clear();
            driver.FindElement(searchInputBox).SendKeys("ECBST33BN");
            driver.FindElement(By.CssSelector("#main-search-bar > div.input-group.search-box > span.input-group-btn > button.btn.btn-default")).Click();
            driver.FindElement(By.XPath("//div[@id='customer-artwork']/div")).Click();

            wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("artwork-upload-file")));
            driver.FindElement(By.Id("artwork-upload-file")).Clear();

            String tmpFileToUpload = Path.Combine(filesToUpload, "Heihei.png");
            driver.FindElement(By.Id("artwork-upload-file")).SendKeys(tmpFileToUpload);

            wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"tab-current-artwork\"]/div/div[2]/button")));
            Thread.Sleep(2500);
            driver.FindElement(By.XPath("//*[@id=\"tab-current-artwork\"]/div/div[2]/button")).Click();
            
            BringElementIntoView(By.XPath("//div[@id='add-to-cart-partial']/div[3]/form/div/button"));

            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.CssSelector("modal-backdrop fade in")));
            wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("artwork-notes")));

            driver.FindElement(By.Id("artwork-notes")).Clear();
            driver.FindElement(By.Id("artwork-notes")).SendKeys("This is a not");

            wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"add-to-cart-form\"]/div/input")));
            driver.FindElement(By.XPath("//*[@id=\"add-to-cart-form\"]/div/input")).Clear();
            driver.FindElement(By.XPath("//*[@id=\"add-to-cart-form\"]/div/input")).SendKeys("2");

            driver.FindElement(By.XPath("//div[@id='add-to-cart-partial']/div[3]/form/div/button")).Click();
          
            wait.Until(ExpectedConditions.ElementToBeClickable((By.XPath("//div[@id='shoppingcart-add-container']/div/div[2]/div/a"))));

            driver.FindElement(By.XPath("//div[@id='shoppingcart-add-container']/div/div[2]/div/a")).Click();
            driver.FindElement(By.Id("ShoppingCart_LineItems_0__Quantity")).Clear();
            driver.FindElement(By.Id("ShoppingCart_LineItems_0__Quantity")).SendKeys("3");
            driver.FindElement(By.Id("ShoppingCart_LineItems_1__Quantity")).Clear();
            driver.FindElement(By.Id("ShoppingCart_LineItems_1__Quantity")).SendKeys("6");
            driver.FindElement(By.CssSelector("button.btn.btn-primary")).Click();
            driver.FindElement(By.CssSelector("div.col-md-6 > div.checkout-options > a.btn.btn-warning")).Click();
            driver.FindElement(By.XPath("(//button[@type='submit'])[3]")).Click();
            driver.FindElement(By.Id("BillFirstName")).Clear();
            driver.FindElement(By.Id("BillFirstName")).SendKeys("Johnny");
            driver.FindElement(By.Id("BillLastName")).Clear();
            driver.FindElement(By.Id("BillLastName")).SendKeys("Test");
            driver.FindElement(By.Id("BillAddress1")).Clear();
            driver.FindElement(By.Id("BillAddress1")).SendKeys("81 Commerce Dr");
            driver.FindElement(By.Id("BillCity")).Clear();
            driver.FindElement(By.Id("BillCity")).SendKeys("Fal River");
            new SelectElement(driver.FindElement(By.Id("BillStateProvince"))).SelectByText("Maine");
            new SelectElement(driver.FindElement(By.Id("BillStateProvince"))).SelectByText("Maryland");
            new SelectElement(driver.FindElement(By.Id("BillStateProvince"))).SelectByText("Massachusetts");
            driver.FindElement(By.Id("BillPostalCode")).Clear();
            driver.FindElement(By.Id("BillPostalCode")).SendKeys("02720");
            driver.FindElement(By.Id("BillPostalCode")).Clear();
            driver.FindElement(By.Id("BillPostalCode")).SendKeys("02720");
            driver.FindElement(By.Id("BillPhoneNumber")).Clear();
            driver.FindElement(By.Id("BillPhoneNumber")).SendKeys("800-572-2194");
            driver.FindElement(By.Id("EmailAddress")).Clear();
            driver.FindElement(By.Id("EmailAddress")).SendKeys("info@displays2go.com");
            driver.FindElement(By.XPath("(//button[@type='submit'])[3]")).Click();
            driver.FindElement(By.Id("ShipAddressClassificationTypeResidental")).Click();
            driver.FindElement(By.XPath("(//button[@type='submit'])[3]")).Click();
            driver.FindElement(By.XPath("(//input[@id='ShipMethod'])[2]")).Click();

            ResetCapthaRedisKey("redis-gpaweb.awsdev.d2gstores.com", 6379);

            driver.FindElement(By.XPath("(//button[@type='submit'])[3]")).Click();

            Random rnd = new Random();
            Int32 rndMonth = rnd.Next(1, 13);
            Int32 rndYear = DateTime.Now.Year + 1;

            driver.FindElement(By.Id("AccountNumber")).Click();
            driver.FindElement(By.Id("AccountNumber")).Clear();
            driver.FindElement(By.Id("AccountNumber")).SendKeys("4111111111111111");
            driver.FindElement(By.Id("NameOnCard")).Clear();
            driver.FindElement(By.Id("NameOnCard")).SendKeys("Tony Stark");

            new SelectElement(driver.FindElement(By.Id("ExpirationMonth"))).SelectByText($"{CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(rndMonth)} ({rndMonth})"); // "April (4)");
            new SelectElement(driver.FindElement(By.Id("ExpirationYear"))).SelectByText(rnd.Next(rndYear, rndYear + 9).ToString());

            driver.FindElement(By.XPath("(//button[@type='submit'])[3]")).Click();

            Thread.Sleep(5000);

            if (driver.Title == "Displays2go | Secure Checkout (Confirm)")
            {
                if ("Review Information And Place Order".Equals(driver.FindElement(By.CssSelector("h1")).Text.ToString()))
                {
                    driver.FindElement(By.Id("Notes")).Click();
                    driver.FindElement(By.Id("Notes")).Clear();
                    driver.FindElement(By.Id("Notes")).SendKeys("Here is my notes");
                    driver.FindElement(By.Id("SecurityCode")).Click();
                    driver.FindElement(By.Id("SecurityCode")).Clear();
                    driver.FindElement(By.Id("SecurityCode")).SendKeys("123");
                    driver.FindElement(By.Id("checkout-order-submit-btn")).Click();

                    Thread.Sleep(5000); 
                    if (driver.Title == "Your order has been received.")
                    {
                        if (!driver.FindElement(By.CssSelector("strong")).Text.ToString().StartsWith("Order Number: WEB"))
                        {
                            throw new Exception();
                        }
                    }               
                    else
                    {
                        throw new Exception();
                    }    
                }
                else
                {
                    throw new Exception("Incorrect value for the h1 tag, should read 'Review Information And Place Order' but reads " + driver.FindElement(By.CssSelector("h1")).Text.ToString());
                }
            }
            else if (driver.FindElement(By.CssSelector("div.validation-summary-errors > span")).Text == "Please correct the following errors and try again.")
            {
                driver.FindElement(By.Id("header-cart-dropdown-menu")).Click();
                driver.FindElement(By.LinkText("View Cart")).Click();
                driver.FindElement(By.XPath("//table[@id='shopping-cart-items']/tbody/tr[2]/td/a")).Click();
                driver.FindElement(By.XPath("//table[@id='shopping-cart-items']/tbody/tr[2]/td/a")).Click();
            }

            driver.Navigate().GoToUrl("http://www.google.com");
        }

        public void SearchAndBuy()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(45));

            #region Login
            driver.Navigate().GoToUrl(baseURL + "/");
            driver.FindElement(By.Id("hlp-login-link")).Click();
            driver.FindElement(By.Id("EmailAddress")).Clear();
            driver.FindElement(By.Id("EmailAddress")).SendKeys(tmpEmailAddress);// "lynchy703@gmail.com");
            driver.FindElement(By.Id("Password")).Clear();
            driver.FindElement(By.Id("Password")).SendKeys(tmpEmailAddPW); // "Displays2GO");
            driver.FindElement(By.Id("RememberMe")).Click();
            driver.FindElement(By.XPath("(//button[@type='submit'])[3]")).Click();
            #endregion

            #region Track a random order
            String webOrderID = GetATrackingNumber();
            driver.FindElement(By.Id("OrderNumber")).Clear();
            driver.FindElement(By.Id("OrderNumber")).SendKeys(webOrderID);
            driver.FindElement(By.Id("EmailAddress")).Clear();
            driver.FindElement(By.Id("EmailAddress")).SendKeys("aTestEmailAddress@aFakeDomain.com");
            driver.FindElement(By.XPath("(//button[@type='submit'])[3]")).Click();

            WaitForElementExistance(By.CssSelector("h4"));
            #endregion          

            driver.FindElement(By.CssSelector("#main-search-bar > div.input-group.search-box > span.twitter-typeahead > input[name=\"q\"]")).Clear();
            driver.FindElement(By.CssSelector("#main-search-bar > div.input-group.search-box > span.twitter-typeahead > input[name=\"q\"]")).SendKeys("LCK3072MPS");
            driver.FindElement(By.CssSelector("#main-search-bar > div.input-group.search-box > span.input-group-btn > button.btn.btn-default")).Click();
            
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.Id("sku"), "LCK3072MPS"));

            wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"add-to-cart-form\"]/div/input")));
            driver.FindElement(By.XPath("//*[@id=\"add-to-cart-form\"]/div/input")).Clear();
            driver.FindElement(By.XPath("//*[@id=\"add-to-cart-form\"]/div/input")).SendKeys("4");

            driver.FindElement(By.XPath("//*[@id=\"add-to-cart-form\"]/div/button")).Click();

            By loadingImage = By.XPath("//div[@id='main-product-media-outer']/div[2]");
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(loadingImage));

            driver.FindElement(By.XPath("(//button[@type='submit'])[3]")).Click();

            //Thread.Sleep(4000);
            WaitForElementExistance(By.XPath("//div[@id='shoppingcart-add-container']/div/div[2]/div/a"));
            driver.FindElement(By.XPath("//div[@id='shoppingcart-add-container']/div/div[2]/div/a")).Click();

            if (driver.Title == "Displays2go | Secure Checkout(Billing Address)")
                driver.FindElement(By.Name("BillingAddressID")).Click();

            driver.FindElement(By.Name("PostalCode")).Click();
            driver.FindElement(By.Name("PostalCode")).Clear();
            driver.FindElement(By.Name("PostalCode")).SendKeys("02809");
            driver.FindElement(By.XPath("(//button[@type='submit'])[5]")).Click();
            driver.FindElement(By.XPath("//table[@id='shipping-options-table']/tbody/tr[2]/td[4]/a")).Click();
            driver.FindElement(By.CssSelector("div.col-md-6 > div.checkout-options > a.btn.btn-warning")).Click();

            driver.FindElement(By.Id("ShipCompanyName")).Clear();
            driver.FindElement(By.Id("ShipCompanyName")).SendKeys("My Company");
            driver.FindElement(By.Id("ShipFirstName")).Clear();
            driver.FindElement(By.Id("ShipFirstName")).SendKeys("John");
            driver.FindElement(By.Id("ShipLastName")).Clear();
            driver.FindElement(By.Id("ShipLastName")).SendKeys("test");
            driver.FindElement(By.Id("ShipAddress1")).Clear();
            driver.FindElement(By.Id("ShipAddress1")).SendKeys("81 commerce dr");
            driver.FindElement(By.Id("ShipCity")).Clear();
            driver.FindElement(By.Id("ShipCity")).SendKeys("Fall River");
            new SelectElement(driver.FindElement(By.Id("ShipStateProvince"))).SelectByText("Massachusetts");
            driver.FindElement(By.Id("ShipPostalCode")).Clear();
            driver.FindElement(By.Id("ShipPostalCode")).SendKeys("02720");
            driver.FindElement(By.Id("ShipPhoneNumber")).Clear();
            driver.FindElement(By.Id("ShipPhoneNumber")).SendKeys("508-555-1234");

            driver.FindElement(By.XPath("(//button[@type='submit'])[3]")).Click();

            driver.FindElement(By.Id("ShipAddressClassificationTypeResidental")).Click();
            driver.FindElement(By.Id("ShipCompanyName")).Clear();
            driver.FindElement(By.Id("ShipCompanyName")).SendKeys("Banana Republic LLC");
            driver.FindElement(By.XPath("(//button[@type='submit'])[3]")).Click();

            ResetCapthaRedisKey("redis-gpaweb.awsdev.d2gstores.com", 6379);

            driver.FindElement(By.XPath("(//button[@type='submit'])[3]")).Click();

            WaitForElementExistance(By.Id("AccountNumber"));

            Random rnd = new Random();
            Int32 rndMonth = rnd.Next(1, 13);
            Int32 rndYear = DateTime.Now.Year + 1;

            WaitForElementExistance(By.Id("AccountNumber"));
            driver.FindElement(By.Id("AccountNumber")).Clear();
            driver.FindElement(By.Id("AccountNumber")).SendKeys("4111111111111111");
            driver.FindElement(By.Id("NameOnCard")).Clear();
            driver.FindElement(By.Id("NameOnCard")).SendKeys("Tony Stark");

            new SelectElement(driver.FindElement(By.Id("ExpirationMonth"))).SelectByText($"{CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(rndMonth)} ({rndMonth})"); // "April (4)");
            new SelectElement(driver.FindElement(By.Id("ExpirationYear"))).SelectByText(rnd.Next(rndYear, rndYear + 9).ToString());
            driver.FindElement(By.XPath("(//button[@type='submit'])[3]")).Click();

            driver.FindElement(By.CssSelector("#header-cart-dropdown-menu > span.caret")).Click();
            driver.FindElement(By.XPath("//li[@id='nav-cart']/ul/li[2]/div[2]/a/i")).Click();
            driver.FindElement(By.XPath("//table[@id='shopping-cart-items']/tbody/tr[2]/td/a")).Click();
            driver.FindElement(By.Id("account-dropdown")).Click();

            WaitForElementExistance(By.LinkText("Log Out"));
            driver.FindElement(By.LinkText("Log Out")).Click();
            driver.Navigate().GoToUrl("http://www.google.com");
        }

        public void writeProductReviews()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(45));
            String tmpFileToUpload = Path.Combine(filesToUpload, "Heihei.png");

            #region Login
            driver.Navigate().GoToUrl(baseURL + "/");
            driver.FindElement(By.Id("hlp-login-link")).Click();
            driver.FindElement(By.Id("EmailAddress")).Clear();
            driver.FindElement(By.Id("EmailAddress")).SendKeys(tmpEmailAddress);// "lynchy703@gmail.com");
            driver.FindElement(By.Id("Password")).Clear();
            driver.FindElement(By.Id("Password")).SendKeys(tmpEmailAddPW); // "Displays2GO");
            driver.FindElement(By.Id("RememberMe")).Click();
            driver.FindElement(By.XPath("(//button[@type='submit'])[3]")).Click();
            #endregion

            #region start review process

            #region  Ensure we grab an order we have not already written a review for
            Random rnd = new Random();
            while (true)
            {
                String randDate = rnd.Next(1, 12).ToString() + "/" + rnd.Next(1, 28).ToString() + "/" + "2017";
                String prodId = getARandomProductId(randDate);
                String order = getAnOrderNumber(Convert.ToInt32(prodId), randDate);
                if(!isReviewedAlready(Convert.ToInt32(prodId), order))
                {
                    _prodId = prodId;
                    _order = order;
                    break;                    
                }
            }
            #endregion

            WaitForElementExistance(By.CssSelector("#main-search-bar > div.input-group.search-box > span.twitter-typeahead > input[name=\"q\"]"));
            driver.FindElement(By.CssSelector("#main-search-bar > div.input-group.search-box > span.twitter-typeahead > input[name=\"q\"]")).Clear();
            driver.FindElement(By.CssSelector("#main-search-bar > div.input-group.search-box > span.twitter-typeahead > input[name=\"q\"]")).SendKeys(_prodId);
            driver.FindElement(By.CssSelector("#main-search-bar > div.input-group.search-box > span.input-group-btn > button.btn.btn-default")).Click();

            WaitForElementExistance(By.LinkText("Write a Review"));
            driver.FindElement(By.LinkText("Write a Review")).Click();
            #endregion

            #region  Populate the viewModel  
            driver.FindElement(By.XPath("//div[@id='review-rating']/i[5]")).Click();
            driver.FindElement(By.Id("OrderNumber")).Click();
            driver.FindElement(By.Id("OrderNumber")).Clear();
            driver.FindElement(By.Id("OrderNumber")).SendKeys("12345678");
            driver.FindElement(By.Id("Title")).Click();
            driver.FindElement(By.Id("Title")).Clear();
            driver.FindElement(By.Id("Title")).SendKeys("1234");
            driver.FindElement(By.Id("Text")).Click();
            driver.FindElement(By.Id("Text")).Clear();
            driver.FindElement(By.Id("Text")).SendKeys("4321");
            driver.FindElement(By.Id("user-content-input")).Clear();
            driver.FindElement(By.Id("user-content-input")).SendKeys(tmpFileToUpload);
            Thread.Sleep(3000);
            driver.FindElement(By.XPath("(//button[@type='submit'])[3]")).Click();

            //Ensure Select statemnt succeeded, if not just log out
            if(_order != null)
            {
                driver.FindElement(By.Id("OrderNumber")).Click();
                driver.FindElement(By.Id("OrderNumber")).Clear();
                driver.FindElement(By.Id("OrderNumber")).SendKeys(_order);
                driver.FindElement(By.Id("Title")).Click();
                driver.FindElement(By.Id("Title")).Clear();
                driver.FindElement(By.Id("Title")).SendKeys("Better than advertised!");
                driver.FindElement(By.Id("Text")).Click();
                driver.FindElement(By.Id("Text")).Clear();
                driver.FindElement(By.Id("Text")).SendKeys("This product made bingo night at the VFW that much better!");
                Thread.Sleep(3000);
                driver.FindElement(By.XPath("(//button[@type='submit'])[3]")).Click();
            }            
            #endregion

            #region logout, return to google
            driver.FindElement(By.Id("account-dropdown")).Click();
            WaitForElementExistance(By.LinkText("Log Out"));
            driver.FindElement(By.LinkText("Log Out")).Click();
            driver.Navigate().GoToUrl("http://www.google.com");
            #endregion
        }
        #endregion

        #region Helper Methods
        //Verify that this product/order/ipAddress combination has NOT been reviewed yet via the selenium app
        //return true if this product has already been reviewed and should seek another random order
        public bool isReviewedAlready(int productId, string order)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["UserContentContext"].ConnectionString))
            {
                conn.Open();
                var sql = "SELECT [ProductID] FROM [UserContent].[dbo].[Reviews] WHERE [OrderNumber] = '" + order + "' AND [ProductID] = " + productId + ";";
                var cmd = new SqlCommand(sql, conn);
                try
                {
                    String result = cmd.ExecuteScalar().ToString();
                    return true;
                }
                catch
                {
                    return false;
                }
            }            
        }

        private void VerifyEmailAddress()
        {
            using (SqlConnection dbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["CheckoutContextUAT"].ConnectionString))
            {
                dbConnection.Open();

                String confirmEmailStr = "Update Customers SET IsVerified = 1 WHERE EmailAddress = @EmailAddress";

                SqlCommand selectCmd = new SqlCommand(confirmEmailStr, dbConnection) { CommandTimeout = 300 };
                selectCmd.Parameters.AddWithValue("@EmailAddress", tmpEmailAddress);

                Int32 recordsFnd = selectCmd.ExecuteNonQuery();
            }


            //Flip switch
            driver.Navigate().GoToUrl(baseURL + "/Account");
            Thread.Sleep(2000);
        }

        //Returns a random prodict ID from a random web order from last year
        public string getARandomProductId(string randDate)
        {
            string Id;

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["GPANAVToWebSyncDev"].ConnectionString))
            {                
                conn.Open();
                var sql = "SELECT DISTINCT TOP (1) [ProductId], [DateCreated] FROM [NAVToWebSync].[dbo].[OrderItems] WHERE [OrderNumber] LIKE 'WEB%' AND [DateCreated] <= '" + randDate + "' order by [DateCreated] DESC;";
                var cmd = new SqlCommand(sql, conn);
                Id = cmd.ExecuteScalar().ToString();
            }
            return Id;
        }

        //Return a random web order number
        public string getAnOrderNumber(int Id, string randDate)
        {
            string anOrder;
            using(SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["GPANAVToWebSyncDev"].ConnectionString))
            {
                Random rnd = new Random();
                conn.Open();
                var sql = "SELECT DISTINCT TOP (1) [OrderNumber], [DateCreated] FROM [NAVToWebSync].[dbo].[OrderItems] where [OrderNumber] LIKE 'WEB%' AND [ProductId] = " + Id + " AND [DateCreated] <= '" + randDate + "' order by [DateCreated] DESC;";
                var cmd = new SqlCommand(sql, conn);
                anOrder = (string)cmd.ExecuteScalar();
            }
            return anOrder;
        }

        private String GetATrackingNumber()
        {
            String webOrderNumberToTrack;

            using (SqlConnection dbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["WebSyncDb"].ConnectionString))
            {
                Random rnd = new Random();

                dbConnection.Open();

                String confirmEmailStr = "SELECT [OrderNumber], [DateUpdated] FROM [NAVToWebSync].[dbo].[Shipments] WHERE (OrderNumber LIKE 'WEB%') AND [DateUpdated] < '" + DateTime.Now.AddDays((rnd.Next(40, 300) * -1)).ToShortDateString() + "' ORDER BY [DateUpdated] DESC";
                //String confirmEmailStr = "SELECT DISTINCT TOP 1 [OrderNumber],[DateUpdated] FROM [Shipments] WHERE (OrderNumber LIKE 'WEB%') AND IsNotified = 1 and CarrierCode = 'FDX' ORDER BY DateUpdated DESC";

                SqlCommand selectCmd = new SqlCommand(confirmEmailStr, dbConnection) { CommandTimeout = 300 };
                webOrderNumberToTrack = (String)selectCmd.ExecuteScalar();
            }

            //Flip switch
            driver.Navigate().GoToUrl(baseURL + "/Tracking");

            return webOrderNumberToTrack;
        }

        //Removes the lockout and payment keys from redis, so that the Recaptcha controls do not impede progress
        private void ResetCapthaRedisKey(String Endpoint, Int32 port)
        {
            ConfigurationOptions defaultConfigOptions = new ConfigurationOptions
            {
                EndPoints =
                {
                    { Endpoint, port }
                },
                KeepAlive = 60,
                AllowAdmin = true,
                //Ssl = RedisDBConfigurations.DefaultUseSSL,
                DefaultDatabase = 9,
                //Password = RedisDBConfigurations.DefaultPassword,
                ConnectTimeout = 5000, // 5 sec
                SyncTimeout = 60000, // 60 sec - to allow for clearing cache with client's with large caches
                ConnectRetry = 10
            };

            using (ConnectionMultiplexer redisConnection = ConnectionMultiplexer.Connect(defaultConfigOptions))
            {
                StackExchangeRedisCacheClient cacheExtClient = new StackExchangeRedisCacheClient(redisConnection, new NewtonsoftSerializer(), redisConnection.GetDatabase().Database);

                List<String> keys = (cacheExtClient.SearchKeys("*")).ToList();
                if (keys.Count > 0)
                    cacheExtClient.FlushDb();
                
                if (baseURL.Equals("https://www.displays2go.com", StringComparison.CurrentCultureIgnoreCase))
                {
                    String abortCaptchaKey = $"Captcha:AbortCaptchaKey{publicIp}";
                    cacheExtClient.Add(abortCaptchaKey, "IgnoreCaptcha", DateTime.UtcNow.AddMinutes(30));
                }
            }

            //if (baseURL.Equals("https://www.displays2go.com", StringComparison.CurrentCultureIgnoreCase))
            //{
            //    ConfigurationOptions defaultConfigOptions = new ConfigurationOptions
            //    {
            //        EndPoints =
            //    {
            //        { Endpoint, port }
            //    },
            //        KeepAlive = 60,
            //        AllowAdmin = true,
            //        //Ssl = RedisDBConfigurations.DefaultUseSSL,
            //        DefaultDatabase = 9,
            //        //Password = RedisDBConfigurations.DefaultPassword,
            //        ConnectTimeout = 5000, // 5 sec
            //        SyncTimeout = 60000, // 60 sec - to allow for clearing cache with client's with large caches
            //        ConnectRetry = 10
            //    };

            //    using (ConnectionMultiplexer redisConnection = ConnectionMultiplexer.Connect(defaultConfigOptions))
            //    {
            //        StackExchangeRedisCacheClient cacheExtClient = new StackExchangeRedisCacheClient(redisConnection, new NewtonsoftSerializer(), redisConnection.GetDatabase().Database);

            //        List<String> keys = (cacheExtClient.SearchKeys("*")).ToList();
            //        if (keys.Count > 0)
            //            cacheExtClient.FlushDb();

            //        String abortCaptchaKey = $"Captcha:AbortCaptchaKey{publicIp}";
            //        cacheExtClient.Add(abortCaptchaKey, "IgnoreCaptcha", DateTime.UtcNow.AddMinutes(30));
            //    }
            //}
        }

        public String GetPublicIPAddress()
        {
            driver.Navigate().GoToUrl("https://www.google.com/search?q=what+is+my+ip&rlz=1C1GCEA_enUS782US782&oq=what+is+m&aqs=chrome.2.69i57j69i60j0l4.3671j0j7&sourceid=chrome&ie=UTF-8");
            return driver.FindElement(By.XPath("//*[@id=\"rso\"]/div[1]/div/div/div[1]/w-answer/w-answer-desktop/div[1]")).Text;
        }

        public String GetLocalIPAddress()
        {
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }

        private Boolean WaitForElementExistance(By elementToFind)
        {
            DateTime startTime = DateTime.UtcNow;
            while (DateTime.UtcNow - startTime < TimeSpan.FromSeconds(60))
            {
                try
                {
                    if (IsElementPresent(elementToFind))
                    {
                        Thread.Sleep(1000);
                        return true;
                    }
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }

            throw new NoSuchElementException($"{elementToFind.ToString()} can not be found!");
            //return false;
        }

        private void BringElementIntoView(By ElementToFind)
        {
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView(false)", driver.FindElement(ElementToFind));
        }

        private bool IsElementPresent(By by)
        {
            try
            {
                IWebElement eleDetails = driver.FindElement(by);
                if (eleDetails.Displayed && eleDetails.Enabled)
                    return true;
                else
                    return false;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }
        #endregion
    }
}
